﻿using System;
using System.Collections.Generic;

namespace ConsoleApp
{
    public class Heap<T> where T:IComparable, new()
    {
        public List<T> Items { get; }

        public Heap() {
            Items = new List<T>();
        }

        public Heap(List<T> list) {
            Items = list;
            BuildHeap();
        }

        private void Up(int index) {
            while (index != 0 && Items[index].CompareTo(Items[(index - 1) / 2]) < 0) {
                T temp = Items[index];
                Items[index] = Items[(index - 1) / 2];
                Items[(index - 1) / 2] = temp;
                
                index = (index - 1) / 2;
            }
        }

        private void Down(int index) {
            int minIndex = index;
            int leftSon = index * 2 + 1;
            int rightSon = index * 2 + 2;
            if (leftSon < Items.Count && Items[leftSon].CompareTo(Items[minIndex]) < 0) {
                minIndex = leftSon;
            }
            if (rightSon < Items.Count && Items[rightSon].CompareTo(Items[minIndex]) < 0) {
                minIndex = rightSon;
            }
            if (minIndex != index) {
                T temp = Items[index];
                Items[index] = Items[minIndex];
                Items[minIndex] = temp;
                Down(minIndex);
            }
        }

        public void Add(T number) {
            Items.Add(number);
            Up(Items.Count - 1);
        }

        public void Delete(int index) {
            if (Items.Count > 0)
            {
                Items[index] = Items[Items.Count - 1];
                Items.RemoveAt(Items.Count - 1);
                Down(index);
            }
        }

        private void BuildHeap()
        {
            for (int i = Items.Count / 2; i >= 0; i--) {
                Down(i);
            }
        }

        public T GetKthInSize(int index)
        {
            T res = new T();
            Heap<IndexedValue<T>> heap = new Heap<IndexedValue<T>>();

            if (index > Items.Count || index < 0) {
                return res;
            }

            heap.Add(new IndexedValue<T>(Items[0], 0)); //root

            for (int i = 0; i < index; i++) {
                IndexedValue<T> indexedValue = heap.Items[0];
                int leftSon = indexedValue.Index * 2 + 1;
                int rightSon = indexedValue.Index * 2 + 2;

                heap.Delete(0);

                if (leftSon < Items.Count) {
                    heap.Add(new IndexedValue<T>(Items[leftSon], leftSon));
                }
                if (rightSon < Items.Count) {
                    heap.Add(new IndexedValue<T>(Items[rightSon], rightSon));
                }
            }

            return heap.Items[0].Value;
        }
    }
}
