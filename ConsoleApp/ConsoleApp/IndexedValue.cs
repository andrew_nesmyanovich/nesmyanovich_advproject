﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class IndexedValue<T> : IComparable where T : IComparable, new()
    {
        public int Index { set; get; }
        public T Value { set; get; }

        public IndexedValue() { }

        public IndexedValue(T value, int index) {
            Value = value;
            Index = index;
        }

        public int CompareTo(object obj)
        {
            return Value.CompareTo((obj as IndexedValue<T>).Value);
        }
    }
}
