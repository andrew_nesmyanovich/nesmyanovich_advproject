﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();


            
            try
            {
                StreamReader sr = new StreamReader("tests\\Test1.txt");

                string firstLine = sr.ReadLine();

                int size = Convert.ToInt32(firstLine.Split(' ')[0]);
                int kIndex = Convert.ToInt32(firstLine.Split(' ')[1]);

                List<int> input = new List<int>();
                for (int i = 0; i < size; i++) {
                    input.Add(Convert.ToInt32(sr.ReadLine()));
                }

                Heap<int> heap = new Heap<int>(input);

                Console.WriteLine(heap.GetKthInSize(kIndex));

                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Incorrect input");
            }

            Console.ReadKey();

            //for (int i = 0; i < 5; i++)
            //{
            //    List<int> list = new List<int>();
            //    int heapSize = 100000;

            //    int num = random.Next() % heapSize;

            //    for (int j = 0; j < heapSize; j++)
            //    {
            //        list.Add(random.Next());
            //    }
            //    Heap<int> heap = new Heap<int>(list);

            //    StreamWriter writer = new StreamWriter($"tests\\BigTest{i + 1}.txt");
            //    writer.WriteLine($"{heapSize} {num}");
            //    for (int count = 0; count < heapSize; count++)
            //    {
            //        writer.WriteLine(list[count]);
            //    }
            //    writer.Close();

            //    StreamWriter writer1 = new StreamWriter($"tests\\BigTest{i + 1}_ans.txt");
            //    writer1.WriteLine(heap.GetKthInSize(num));
            //    writer1.Close();
            //}

            //StreamWriter writer1 = new StreamWriter($"tests\\test.txt");
            //writer1.WriteLine("1");
            //writer1.WriteLine("1");
            //writer1.WriteLine("1");
            //writer1.Close();
            //StreamReader sr = new StreamReader($"tests\\test.txt");

            //int i = 1;
            //string line = sr.ReadLine();
            //while (line != null)
            //{
            //    Console.WriteLine(line);
            //    line = sr.ReadLine();
            //}
            //Console.WriteLine(i);
            //sr.Close();
            Console.ReadKey();
        }

    }
}
