﻿using System;
using System.Linq;
using System.Windows;
using GraphX.PCL.Common.Enums;
using GraphX.PCL.Logic.Algorithms.LayoutAlgorithms;
using GraphX.Controls;
using SimpleGraph.Models;
using System.Windows.Media;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Threading;

/* Some notes about the main objects and types in this example:
 * 
 * GraphAreaExample - our custom layout panel derived from the base GraphArea class with custom data types. Presented as object named: Area.
 * GraphExample - our custom data graph derived from BidirectionalGraph class using custom data types.
 * GXLogicCoreExample - our custom logic core that contains all logic settings and algorithms
 * DataVertex - our custom vertex data type.
 * DataEdge - our custom edge data type.
 * Zoombox - zoom control object that handles all zooming and interaction stuff. Presented as object named: zoomctrl.
 * 
 * VertexControl - visual vertex object that is responsible for vertex drawing. Can be found in Area.VertexList collection.
 * EdgeControl - visual edge object that is responsible for edge drawing. Can be found in Area.EdgesList collection.
 */

namespace ADVWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window, IDisposable
    {
        private bool Animation { set; get; }
        //private static List<ManualResetEvent> waitHandle = new List<ManualResetEvent>();

        public MainWindow()
        {
            InitializeComponent();
            Animation = true;
            //Customize Zoombox a bit
            //Set minimap (overview) window to be visible by default
            ZoomControl.SetViewFinderVisibility(zoomctrl, Visibility.Visible);
            //Set Fill zooming strategy so whole graph will be always visible
            zoomctrl.ZoomToFill();

            //Lets setup GraphArea settings
            GraphAreaExample_Setup();

            //gg_but_randomgraph.Click += gg_but_randomgraph_Click;
            //gg_but_relayout.Click += gg_but_relayout_Click;
            Area.SetEdgesDashStyle(EdgeDashStyle.Solid);
                        
            Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //lets create graph
            //Note that you can't create it in class constructor as there will be problems with visuals
            gg_but_randomgraph_Click(null, null);
        }

        void gg_but_relayout_Click(object sender, RoutedEventArgs e)
        {
            //This method initiates graph relayout process which involves consequnet call to all selected algorithms.
            //It behaves like GenerateGraph() method except that it doesn't create any visual object. Only update existing ones
            //using current Area.Graph data graph.
            Area.RelayoutGraph();
            zoomctrl.ZoomToFill();
        }

        void gg_but_randomgraph_Click(object sender, RoutedEventArgs e)
        {
            //Lets generate configured graph using pre-created data graph assigned to LogicCore object.
            //Optionaly we set first method param to True (True by default) so this method will automatically generate edges
            //  If you want to increase performance in cases where edges don't need to be drawn at first you can set it to False.
            //  You can also handle edge generation by calling manually Area.GenerateAllEdges() method.
            //Optionaly we set second param to True (True by default) so this method will automaticaly checks and assigns missing unique data ids
            //for edges and vertices in _dataGraph.
            //Note! Area.Graph property will be replaced by supplied _dataGraph object (if any).
            Area.GenerateGraph(true, true);

            /* 
             * After graph generation is finished you can apply some additional settings for newly created visual vertex and edge controls
             * (VertexControl and EdgeControl classes).
             * 
             */

            //This method sets the dash style for edges. It is applied to all edges in Area.EdgesList. You can also set dash property for
            //each edge individually using EdgeControl.DashStyle property.
            //For ex.: Area.EdgesList[0].DashStyle = GraphX.EdgeDashStyle.Dash;


            //This method sets edges arrows visibility. It is also applied to all edges in Area.EdgesList. You can also set property for
            //each edge individually using property, for ex: Area.EdgesList[0].ShowArrows = true;
            Area.ShowAllEdgesArrows(false);

            //This method sets edges labels visibility. It is also applied to all edges in Area.EdgesList. You can also set property for
            //each edge individually using property, for ex: Area.EdgesList[0].ShowLabel = true;
            Area.ShowAllEdgesLabels(true);

            zoomctrl.ZoomToFill();
        }

        private GraphExample GraphExample_Setup()
        {
            var dataGraph = new GraphExample();

            return dataGraph;
        }

        private void GraphAreaExample_Setup()
        {
            var logicCore = new GXLogicCoreExample() { Graph = GraphExample_Setup() };
            //This property sets layout algorithm that will be used to calculate vertices positions
            //Different algorithms uses different values and some of them uses edge Weight property.
            logicCore.DefaultLayoutAlgorithm = LayoutAlgorithmTypeEnum.Sugiyama;
            //Now we can set parameters for selected algorithm using AlgorithmFactory property. This property provides methods for
            //creating all available algorithms and algo parameters.
            logicCore.DefaultLayoutAlgorithmParams = logicCore.AlgorithmFactory.CreateLayoutParameters(LayoutAlgorithmTypeEnum.Sugiyama);
            //Unfortunately to change algo parameters you need to specify params type which is different for every algorithm.
            ((SugiyamaLayoutParameters)logicCore.DefaultLayoutAlgorithmParams).VerticalGap = 50;
            //((SugiyamaLayoutParameters)logicCore.DefaultLayoutAlgorithmParams).BaryCenteringByPosition = true;
            ((SugiyamaLayoutParameters)logicCore.DefaultLayoutAlgorithmParams).PositionCalculationMethod = PositionCalculationMethodTypes.IndexBased;
            ((SugiyamaLayoutParameters)logicCore.DefaultLayoutAlgorithmParams).DirtyRound = false;

            //This property sets async algorithms computation so methods like: Area.RelayoutGraph() and Area.GenerateGraph()
            //will run async with the UI thread. Completion of the specified methods can be catched by corresponding events:
            //Area.RelayoutFinished and Area.GenerateGraphFinished.
            logicCore.AsyncAlgorithmCompute = false;

            //Finally assign logic core to GraphArea object
            Area.LogicCore = logicCore;
            var logicCore1 = new GXLogicCoreExample() { Graph = GraphExample_Setup() };
            //This property sets layout algorithm that will be used to calculate vertices positions
            //Different algorithms uses different values and some of them uses edge Weight property.
            logicCore1.DefaultLayoutAlgorithm = LayoutAlgorithmTypeEnum.Sugiyama;
            //Now we can set parameters for selected algorithm using AlgorithmFactory property. This property provides methods for
            //creating all available algorithms and algo parameters.
            logicCore1.DefaultLayoutAlgorithmParams = logicCore1.AlgorithmFactory.CreateLayoutParameters(LayoutAlgorithmTypeEnum.Sugiyama);
            //Unfortunately to change algo parameters you need to specify params type which is different for every algorithm.
            ((SugiyamaLayoutParameters)logicCore1.DefaultLayoutAlgorithmParams).VerticalGap = 50;
            //((SugiyamaLayoutParameters)logicCore.DefaultLayoutAlgorithmParams).BaryCenteringByPosition = true;
            ((SugiyamaLayoutParameters)logicCore1.DefaultLayoutAlgorithmParams).PositionCalculationMethod = PositionCalculationMethodTypes.IndexBased;
            ((SugiyamaLayoutParameters)logicCore1.DefaultLayoutAlgorithmParams).DirtyRound = false;

            //This property sets async algorithms computation so methods like: Area.RelayoutGraph() and Area.GenerateGraph()
            //will run async with the UI thread. Completion of the specified methods can be catched by corresponding events:
            //Area.RelayoutFinished and Area.GenerateGraphFinished.
            logicCore1.AsyncAlgorithmCompute = false;

            //Finally assign logic core to GraphArea object
            Area2.LogicCore = logicCore1;
        }

        public void Dispose()
        {
            //If you plan dynamicaly create and destroy GraphArea it is wise to use Dispose() method
            //that ensures that all potential memory-holding objects will be released.
            Area.Dispose();
        }

        private void InsertButton_Click(object sender, RoutedEventArgs e)
        {
            int number = 0;
            Clean();

            if (Int32.TryParse(NumberBox.Text, out number))
            {
                Insert(Area, ArrayPanel, 
                    new DataVertex(number.ToString()));
            }
            else {
                WrongNumberBorder.BorderBrush = Brushes.Red;
            }

        }


        private async void Down(GraphAreaExample area, StackPanel arrayPanel,
                DataVertex vertex, int index)
        {
            AnimationProcessStart();

            var graph = area.LogicCore.Graph;
            var list = graph.Vertices.ToList();
            int current = index;
            while (true)
            {
                int minIndex = current;
                int leftSon = current * 2 + 1;
                int rightSon = current * 2 + 2;

                DataVertex CurrentVertex = list[current];
                CurrentVertex.Color = Brushes.Blue;

                if (Animation)
                {
                    area.GenerateGraph(true, true);
                    await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                }

                if (leftSon < graph.VertexCount
                    && GetNumber(list[leftSon]) < GetNumber(list[minIndex]))
                {
                    DataVertex prevMinVertex = list[minIndex];
                    prevMinVertex.Color = Brushes.LightGray;
                    minIndex = leftSon;
                    DataVertex nextMinVertex = list[minIndex];
                    nextMinVertex.Color = Brushes.Orange;
                    if (Animation)
                    {
                        area.GenerateGraph(true, true);
                        await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                    }
                }

                if (rightSon < graph.VertexCount
                    && GetNumber(list[rightSon])
                    < GetNumber(list[minIndex]))
                {
                    DataVertex prevMinVertex = list[minIndex];
                    prevMinVertex.Color = Brushes.LightGray;
                    minIndex = rightSon;
                    DataVertex nextMinVertex = list[minIndex];
                    nextMinVertex.Color = Brushes.Orange;
                    if (Animation)
                    {
                        area.GenerateGraph(true, true);
                        await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                    }
                }

                TextBlock parentBlock = (((arrayPanel.Children[current] as StackPanel).Children[0] as Border).Child as TextBlock);
                TextBlock sonBlock = (((arrayPanel.Children[minIndex] as StackPanel).Children[0] as Border).Child as TextBlock);
                DataVertex parentVertex = list[current];
                DataVertex sonVertex = list[minIndex];

                if (minIndex != current)
                {
                    parentBlock.Background = Brushes.Green;
                    sonBlock.Background = Brushes.Green;
                    parentVertex.Color = Brushes.Green;
                    sonVertex.Color = Brushes.Green;

                    if (Animation)
                    {
                        area.GenerateGraph(true, true);
                        await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                    }

                    string temp2 = parentVertex.Text;
                    parentVertex.Text = sonVertex.Text;
                    sonVertex.Text = temp2;

                    string tempText = parentBlock.Text;
                    parentBlock.Text = sonBlock.Text;
                    sonBlock.Text = tempText;

                    parentVertex.Color = Brushes.LightGray;
                    sonVertex.Color = Brushes.Orange;

                    parentBlock.Background = Brushes.White;
                    sonBlock.Background = Brushes.Orange;

                    if (Animation)
                    {
                        area.GenerateGraph(true, true);
                        await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                    }
                }
                else
                {
                    list[current].Color = Brushes.LightGray;
                    parentBlock.Background = Brushes.White;
                    area.GenerateGraph(true, true);
                    break;
                }
                current = minIndex;
            }
            
            AnimationProcessEnd();
            //waitHandle[index].Set();
        }

        private async void Insert(GraphAreaExample area, StackPanel arrayPanel, 
            DataVertex vertex) {

            AnimationProcessStart();

            var graph = area.LogicCore.Graph;
            //Add vertex to data graph
            graph.AddVertex(vertex);
            StackPanel stackPanel = new StackPanel();
            Border border = new Border();
            border.BorderThickness = new Thickness(1);
            border.BorderBrush = Brushes.Black;

            string vertexNumber = "";

            if (vertex.Text.Contains(';'))
            {
                vertexNumber = vertex.Text.Split(';')[0];
            }
            else {
                vertexNumber = vertex.Text;
            }

            border.Child = new TextBlock()
            {
                Text = vertexNumber,
                Width = 50,
                Height = 50,
                Padding = new Thickness(20)
            };

            stackPanel.Children.Add(border);
            stackPanel.Children.Add(new TextBlock()
            {
                Text = (graph.VertexCount - 1).ToString(),
                Foreground = Brushes.Blue,
                Margin = new Thickness(25, 15, 25, 15)
            });
            arrayPanel.Children.Add(stackPanel);

            var list = graph.Vertices.ToList();
            int i = list.Count - 1;
            int parent = (i - 1) / 2;

            //Add edge to parent
            if (graph.VertexCount > 1)
            {
                var dataEdge = new DataEdge(list[parent], list[i]);
                graph.AddEdge(dataEdge);
            }


            //Exchange animation
            while (i != 0 && GetNumber(list[i])
                < GetNumber(list[parent]))
            {

                list[i].Color = Brushes.Green;
                list[parent].Color = Brushes.Green;
                TextBlock curBlock = (((arrayPanel.Children[i] as StackPanel).Children[0] as Border).Child as TextBlock);
                TextBlock parentBlock = (((arrayPanel.Children[parent] as StackPanel).Children[0] as Border).Child as TextBlock);
                curBlock.Background = Brushes.Green;
                parentBlock.Background = Brushes.Green;

                area.GenerateGraph(true, true);
                //Area.RelayoutGraph(false);
                if (Animation)
                {
                    await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                }
                string temp = list[i].Text;
                list[i].Text = list[parent].Text;
                list[parent].Text = temp;

                string tempText = curBlock.Text;
                curBlock.Text = parentBlock.Text;
                parentBlock.Text = tempText;


                if (Animation)
                {
                    area.GenerateGraph(true, true);
                    await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                }

                list[i].Color = Brushes.LightGray;
                list[parent].Color = Brushes.LightGray;
                curBlock.Background = Brushes.White;
                parentBlock.Background = Brushes.White;
                if (Animation)
                {
                    area.GenerateGraph(true, true);
                }

                i = parent;
                parent = (i - 1) / 2;
            }

            area.GenerateGraph(true, true);
            AnimationProcessEnd();
        }

        private void AnimationProcessStart() {
            InsertButton.Click -= InsertButton_Click;
            DeleteButton.Click -= DeleteButton_Click;
            RandomGenerationButton.Click -= Button_Click;
            GetK.Click -= GetK_Click;
            SaveButton.Click -= Save_Click;
            OpenButton.Click -= Open_Click;
            SkipAnimationButton.Click += TurnOffAnimation;
        }

        private void AnimationProcessEnd()
        {
            InsertButton.Click += InsertButton_Click;
            DeleteButton.Click += DeleteButton_Click;
            RandomGenerationButton.Click += Button_Click;
            GetK.Click += GetK_Click;
            SaveButton.Click += Save_Click;
            OpenButton.Click += Open_Click;
            SkipAnimationButton.Click -= TurnOffAnimation;
            Animation = true;
        }

        private int GetNumber(DataVertex vertex) {
            if (vertex.Text.Contains(';'))
            {
                return Convert.ToInt32(vertex.Text.Split(';')[0]);
            }
            else
            {
                return Convert.ToInt32(vertex.Text);
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            int size = 0;
            AnimationProcessStart();
            Clean();

            if (Int32.TryParse(HeapSizeBox.Text, out size) && size < 256 && size > 0)
            {
                List<int> input = new List<int>();
                Random random = new Random();
                for (int i = 0; i < size; i++)
                {
                    input.Add(random.Next() % (size * 5));
                }

                ArrayPanel.Children.RemoveRange(0, ArrayPanel.Children.Count);
                Area.LogicCore.Graph = new GraphExample();
                var graph = Area.LogicCore.Graph;
                //var list = graph.Vertices.ToList();

                for (int counter = 0; counter < input.Count; counter++)
                {
                    var dataVertex1 = new DataVertex(input[counter].ToString());

                    //Add vertex to data graph
                    graph.AddVertex(dataVertex1);
                    StackPanel stackPanel = new StackPanel();
                    Border border = new Border();
                    border.BorderThickness = new Thickness(1);
                    border.BorderBrush = Brushes.Black;
                    border.Child = new TextBlock()
                    {
                        Text = input[counter].ToString(),
                        Width = 50,
                        Height = 50,
                        Padding = new Thickness(20)
                    };

                    stackPanel.Children.Add(border);
                    stackPanel.Children.Add(new TextBlock()
                    {
                        Text = (graph.VertexCount - 1).ToString(),
                        Foreground = Brushes.Blue,
                        Margin = new Thickness(25, 15, 25, 15)
                    });
                    ArrayPanel.Children.Add(stackPanel);
                }

                var list = Area.LogicCore.Graph.Vertices.ToList();

                for (int counter = 0; counter < graph.VertexCount; counter++)
                {
                    int parent1 = (counter - 1) / 2;
                    if (parent1 != counter)
                    {
                        var dataEdge = new DataEdge(list[parent1], list[counter]);
                        graph.AddEdge(dataEdge);
                    }
                }

                Area.GenerateGraph(true, true);

                //for (int index = (graph.VertexCount - 2) / 2 + 1; index >= 0; index--)
                //{
                //    waitHandle.Add(new ManualResetEvent(false));
                //}

                //waitHandle[(graph.VertexCount - 2) / 2 + 1].Set();
                for (int index = (graph.VertexCount - 2) / 2; index >= 0; index--)
                {
                    //waitHandle[index + 1].WaitOne();
                    //Down(Area, ArrayPanel, Area.LogicCore.Graph.Vertices.ToList()[index], index);
                    int current = index;
                    while (true)
                    {
                        int minIndex = current;
                        int leftSon = current * 2 + 1;
                        int rightSon = current * 2 + 2;

                        DataVertex CurrentVertex = list[current];
                        CurrentVertex.Color = Brushes.Blue;

                        if (Animation)
                        {
                            Area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                        }

                        if (leftSon < graph.VertexCount
                            && GetNumber(list[leftSon]) < GetNumber(list[minIndex]))
                        {
                            DataVertex prevMinVertex = list[minIndex];
                            prevMinVertex.Color = Brushes.LightGray;
                            minIndex = leftSon;
                            DataVertex nextMinVertex = list[minIndex];
                            nextMinVertex.Color = Brushes.Orange;
                            if (Animation)
                            {
                                Area.GenerateGraph(true, true);
                                await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                            }
                        }

                        if (rightSon < graph.VertexCount
                            && GetNumber(list[rightSon]) < GetNumber(list[minIndex]))
                        {
                            DataVertex prevMinVertex = list[minIndex];
                            prevMinVertex.Color = Brushes.LightGray;
                            minIndex = rightSon;
                            DataVertex nextMinVertex = list[minIndex];
                            nextMinVertex.Color = Brushes.Orange;
                            if (Animation)
                            {
                                Area.GenerateGraph(true, true);
                                await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                            }
                        }

                        TextBlock parentBlock = (((ArrayPanel.Children[current] as StackPanel).Children[0] as Border).Child as TextBlock);
                        TextBlock sonBlock = (((ArrayPanel.Children[minIndex] as StackPanel).Children[0] as Border).Child as TextBlock);
                        DataVertex parentVertex = list[current];
                        DataVertex sonVertex = list[minIndex];

                        if (minIndex != current)
                        {
                            parentBlock.Background = Brushes.Green;
                            sonBlock.Background = Brushes.Green;
                            parentVertex.Color = Brushes.Green;
                            sonVertex.Color = Brushes.Green;

                            if (Animation)
                            {
                                Area.GenerateGraph(true, true);
                                await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                            }

                            string temp2 = parentVertex.Text;
                            parentVertex.Text = sonVertex.Text;
                            sonVertex.Text = temp2;

                            string tempText = parentBlock.Text;
                            parentBlock.Text = sonBlock.Text;
                            sonBlock.Text = tempText;

                            parentVertex.Color = Brushes.LightGray;
                            sonVertex.Color = Brushes.Orange;

                            parentBlock.Background = Brushes.White;
                            sonBlock.Background = Brushes.Orange;
                            if (Animation)
                            {
                                Area.GenerateGraph(true, true);
                                await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                            }
                        }
                        else
                        {
                            list[current].Color = Brushes.LightGray;
                            parentBlock.Background = Brushes.White;
                            break;
                        }
                        current = minIndex;
                    }
                }
                //waitHandle = new List<ManualResetEvent>();
                Area.GenerateGraph(true, true);
            }
            else {
                WrongSizeBorder.BorderBrush = Brushes.Red;
            }
            AnimationProcessEnd();
        }

        private void SpeedSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SpeedBox.Text = Convert.ToInt32(SpeedSlider.Value).ToString() + "x";
        }

        private void TurnOffAnimation(object sender, RoutedEventArgs e)
        {
            Animation = false;
        }

        private async void GetK_Click(object sender, RoutedEventArgs e)
        {
            Area2.LogicCore.Graph = new GraphExample();
            ArrayPanel2.Children.RemoveRange(0, ArrayPanel2.Children.Count);
            int indexK = 0;
            var graph = Area.LogicCore.Graph;
            Clean();
            AnimationProcessStart();

            if (Int32.TryParse(KIndex.Text, out indexK) && indexK >= 0 && indexK < graph.VertexCount)
            {
                var list1 = graph.Vertices.ToList();

                var graph2 = Area2.LogicCore.Graph;

                //Insert(Area2, ArrayPanel2, new DataVertex(list1[0].Text + ";0"));

                var area = Area2;
                var arrayPanel = ArrayPanel2;
                var vertex = new DataVertex(list1[0].Text + ";0");

                graph = area.LogicCore.Graph;
                var list = graph.Vertices.ToList();
                int i, parent;
                //Add vertex to data graph
                graph.AddVertex(vertex);
                StackPanel stackPanel = new StackPanel();
                Border border = new Border();
                border.BorderThickness = new Thickness(1);
                border.BorderBrush = Brushes.Black;

                string vertexNumber = "";

                if (vertex.Text.Contains(';'))
                {
                    vertexNumber = vertex.Text.Split(';')[0];
                }
                else
                {
                    vertexNumber = vertex.Text;
                }

                border.Child = new TextBlock()
                {
                    Text = vertexNumber,
                    Width = 50,
                    Height = 50,
                    Padding = new Thickness(20)
                };

                stackPanel.Children.Add(border);
                stackPanel.Children.Add(new TextBlock()
                {
                    Text = (graph.VertexCount - 1).ToString(),
                    Foreground = Brushes.Blue,
                    Margin = new Thickness(25, 15, 25, 15)
                });
                arrayPanel.Children.Add(stackPanel);

                Area2.GenerateGraph(true, true);
                Area.GenerateGraph(true, true);
                list1[0].Color = Brushes.Red;


                for (int counter = 0; counter < indexK; counter++)
                {
                    var list2 = graph2.Vertices.ToList();

                    int indexInFirstHeap = Convert.ToInt32(list2[0].Text.Split(';')[1]);
                    //var graph = Area.LogicCore.Graph;

                    //ExtractMin(Area2, ArrayPanel2);
                    area = Area2;
                    graph = area.LogicCore.Graph;
                    list = graph.Vertices.ToList();

                    if (graph.VertexCount > 0)
                    {
                        TextBlock parentBlock = (((arrayPanel.Children[0] as StackPanel).Children[0] as Border).Child as TextBlock);
                        TextBlock sonBlock = (((arrayPanel.Children[graph.VertexCount - 1] as StackPanel).Children[0] as Border).Child as TextBlock);
                        TextBlock parentBlockIndex = ((arrayPanel.Children[0] as StackPanel).Children[1] as TextBlock);

                        DataVertex dataVertex = list[0];
                        DataVertex lastVertex = list[graph.VertexCount - 1];

                        dataVertex.Color = Brushes.Red;
                        lastVertex.Color = Brushes.Yellow;
                        parentBlock.Background = Brushes.Red;
                        sonBlock.Background = Brushes.Yellow;

                        if (Animation)
                        {
                            area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                        }

                        string temp = dataVertex.Text;
                        dataVertex.Text = lastVertex.Text;
                        lastVertex.Text = temp;

                        dataVertex.Color = Brushes.Yellow;
                        lastVertex.Color = Brushes.Red;

                        string tempBlockText = parentBlock.Text;
                        parentBlock.Text = sonBlock.Text;
                        sonBlock.Text = tempBlockText;
                        parentBlockIndex.Text = "0";

                        parentBlock.Background = Brushes.Yellow;
                        sonBlock.Background = Brushes.Red;

                        if (Animation)
                        {
                            Area2.GenerateGraph(true, true);
                            Area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                        }

                        arrayPanel.Children.RemoveAt(graph.VertexCount - 1);
                        graph.RemoveVertex(lastVertex);

                        if (Animation)
                        {
                            Area2.GenerateGraph(true, true);
                            Area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(3000 / SpeedSlider.Value));
                        }


                        if (area.LogicCore.Graph.VertexCount > 1)
                        {
                            //Down(area, arrayPanel, dataVertex, 0);
                            graph = area.LogicCore.Graph;
                            list = graph.Vertices.ToList();
                            int current = 0;
                            while (true)
                            {
                                int minIndex = current;
                                int leftSon = current * 2 + 1;
                                int rightSon = current * 2 + 2;

                                DataVertex CurrentVertex = list[current];
                                CurrentVertex.Color = Brushes.Blue;

                                if (Animation)
                                {
                                    Area2.GenerateGraph(true, true);
                                    Area.GenerateGraph(true, true);
                                    await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                                }

                                if (leftSon < graph.VertexCount
                                    && GetNumber(list[leftSon]) < GetNumber(list[minIndex]))
                                {
                                    DataVertex prevMinVertex = list[minIndex];
                                    prevMinVertex.Color = Brushes.LightGray;
                                    minIndex = leftSon;
                                    DataVertex nextMinVertex = list[minIndex];
                                    nextMinVertex.Color = Brushes.Orange;
                                    if (Animation)
                                    {
                                        Area2.GenerateGraph(true, true);
                                        Area.GenerateGraph(true, true);
                                        await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                                    }
                                }

                                if (rightSon < graph.VertexCount
                                    && GetNumber(list[rightSon])
                                    < GetNumber(list[minIndex]))
                                {
                                    DataVertex prevMinVertex = list[minIndex];
                                    prevMinVertex.Color = Brushes.LightGray;
                                    minIndex = rightSon;
                                    DataVertex nextMinVertex = list[minIndex];
                                    nextMinVertex.Color = Brushes.Orange;
                                    if (Animation)
                                    {
                                        Area2.GenerateGraph(true, true);
                                        Area.GenerateGraph(true, true);
                                        await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                                    }
                                }

                                parentBlock = (((arrayPanel.Children[current] as StackPanel).Children[0] as Border).Child as TextBlock);
                                sonBlock = (((arrayPanel.Children[minIndex] as StackPanel).Children[0] as Border).Child as TextBlock);
                                DataVertex parentVertex = list[current];
                                DataVertex sonVertex = list[minIndex];

                                if (minIndex != current)
                                {
                                    parentBlock.Background = Brushes.Green;
                                    sonBlock.Background = Brushes.Green;
                                    parentVertex.Color = Brushes.Green;
                                    sonVertex.Color = Brushes.Green;

                                    if (Animation)
                                    {
                                        Area2.GenerateGraph(true, true);
                                        Area.GenerateGraph(true, true);
                                        await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                                    }

                                    string temp2 = parentVertex.Text;
                                    parentVertex.Text = sonVertex.Text;
                                    sonVertex.Text = temp2;

                                    string tempText = parentBlock.Text;
                                    parentBlock.Text = sonBlock.Text;
                                    sonBlock.Text = tempText;

                                    parentVertex.Color = Brushes.LightGray;
                                    sonVertex.Color = Brushes.Orange;

                                    parentBlock.Background = Brushes.White;
                                    sonBlock.Background = Brushes.Orange;

                                    if (Animation)
                                    {
                                        Area2.GenerateGraph(true, true);
                                        Area.GenerateGraph(true, true);
                                        await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                                    }
                                }
                                else
                                {
                                    list[current].Color = Brushes.LightGray;
                                    parentBlock.Background = Brushes.White;
                                    Area2.GenerateGraph(true, true);
                                    Area.GenerateGraph(true, true);
                                    break;
                                }
                                current = minIndex;
                            }
                        }

                        Area2.GenerateGraph(true, true);
                        Area.GenerateGraph(true, true);
                    }

                    graph = Area.LogicCore.Graph;
                    list1 = Area.LogicCore.Graph.Vertices.ToList();
                    ////////////////////////////////////////////
                    if (indexInFirstHeap * 2 + 1 < graph.VertexCount)
                    {
                        vertex =
                            new DataVertex($"{list1[indexInFirstHeap * 2 + 1].Text};" +
                            $"{indexInFirstHeap * 2 + 1}");

                        //Insert(Area2, ArrayPanel2, vertexToAdd);
                        area = Area2;
                        arrayPanel = ArrayPanel2;
                        graph = area.LogicCore.Graph;

                        //Add vertex to data graph
                        graph.AddVertex(vertex);
                        stackPanel = new StackPanel();
                        border = new Border();
                        border.BorderThickness = new Thickness(1);
                        border.BorderBrush = Brushes.Black;

                        vertexNumber = "";

                        if (vertex.Text.Contains(';'))
                        {
                            vertexNumber = vertex.Text.Split(';')[0];
                        }
                        else
                        {
                            vertexNumber = vertex.Text;
                        }

                        border.Child = new TextBlock()
                        {
                            Text = vertexNumber,
                            Width = 50,
                            Height = 50,
                            Padding = new Thickness(20)
                        };

                        stackPanel.Children.Add(border);
                        stackPanel.Children.Add(new TextBlock()
                        {
                            Text = (graph.VertexCount - 1).ToString(),
                            Foreground = Brushes.Blue,
                            Margin = new Thickness(25, 15, 25, 15)
                        });
                        arrayPanel.Children.Add(stackPanel);

                        list = graph.Vertices.ToList();
                        i = list.Count - 1;
                        parent = (i - 1) / 2;

                        //Add edge to parent
                        if (graph.VertexCount > 1)
                        {
                            var dataEdge = new DataEdge(list[parent], list[i]);
                            graph.AddEdge(dataEdge);
                        }
                        if (Animation)
                        {
                            Area2.GenerateGraph(true, true);
                            Area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                        }
                        //Exchange animation
                        while (i != 0 && GetNumber(list[i])
                            < GetNumber(list[parent]))
                        {

                            list[i].Color = Brushes.Green;
                            list[parent].Color = Brushes.Green;
                            TextBlock curBlock = (((arrayPanel.Children[i] as StackPanel).Children[0] as Border).Child as TextBlock);
                            TextBlock parentBlock = (((arrayPanel.Children[parent] as StackPanel).Children[0] as Border).Child as TextBlock);
                            curBlock.Background = Brushes.Green;
                            parentBlock.Background = Brushes.Green;

                            
                            //Area.RelayoutGraph(false);
                            if (Animation)
                            {
                                area.GenerateGraph(true, true);
                                await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                            }
                            string temp = list[i].Text;
                            list[i].Text = list[parent].Text;
                            list[parent].Text = temp;

                            string tempText = curBlock.Text;
                            curBlock.Text = parentBlock.Text;
                            parentBlock.Text = tempText;


                            if (Animation)
                            {
                                Area2.GenerateGraph(true, true);
                                Area.GenerateGraph(true, true);
                                await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                            }

                            list[i].Color = Brushes.LightGray;
                            list[parent].Color = Brushes.LightGray;
                            curBlock.Background = Brushes.White;
                            parentBlock.Background = Brushes.White;
                            if (Animation)
                            {
                                Area2.GenerateGraph(true, true);
                                Area.GenerateGraph(true, true);
                            }

                            i = parent;
                            parent = (i - 1) / 2;
                        }

                        Area2.GenerateGraph(true, true);
                        Area.GenerateGraph(true, true);
                        list1[indexInFirstHeap * 2 + 1].Color = Brushes.Red;

                    }

                    graph = Area.LogicCore.Graph;

                    if (indexInFirstHeap * 2 + 2 < graph.VertexCount)
                    {
                        vertex =
                            new DataVertex($"{list1[indexInFirstHeap * 2 + 2].Text};" +
                            $"{indexInFirstHeap * 2 + 2}");

                        //Insert(Area2, ArrayPanel2, vertexToAdd);
                        area = Area2;
                        arrayPanel = ArrayPanel2;

                        graph = area.LogicCore.Graph;
                        //Add vertex to data graph
                        graph.AddVertex(vertex);
                        stackPanel = new StackPanel();
                        border = new Border();
                        border.BorderThickness = new Thickness(1);
                        border.BorderBrush = Brushes.Black;

                        vertexNumber = "";

                        if (vertex.Text.Contains(';'))
                        {
                            vertexNumber = vertex.Text.Split(';')[0];
                        }
                        else
                        {
                            vertexNumber = vertex.Text;
                        }

                        border.Child = new TextBlock()
                        {
                            Text = vertexNumber,
                            Width = 50,
                            Height = 50,
                            Padding = new Thickness(20)
                        };

                        stackPanel.Children.Add(border);
                        stackPanel.Children.Add(new TextBlock()
                        {
                            Text = (graph.VertexCount - 1).ToString(),
                            Foreground = Brushes.Blue,
                            Margin = new Thickness(25, 15, 25, 15)
                        });
                        arrayPanel.Children.Add(stackPanel);

                        list = graph.Vertices.ToList();
                        i = list.Count - 1;
                        parent = (i - 1) / 2;

                        //Add edge to parent
                        if (graph.VertexCount > 1)
                        {
                            var dataEdge = new DataEdge(list[parent], list[i]);
                            graph.AddEdge(dataEdge);
                        }

                        if (Animation)
                        {
                            Area2.GenerateGraph(true, true);
                            Area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                        }

                        //Exchange animation
                        while (i != 0 && GetNumber(list[i])
                            < GetNumber(list[parent]))
                        {

                            list[i].Color = Brushes.Green;
                            list[parent].Color = Brushes.Green;
                            TextBlock curBlock = (((arrayPanel.Children[i] as StackPanel).Children[0] as Border).Child as TextBlock);
                            TextBlock parentBlock = (((arrayPanel.Children[parent] as StackPanel).Children[0] as Border).Child as TextBlock);
                            curBlock.Background = Brushes.Green;
                            parentBlock.Background = Brushes.Green;

                            //Area.RelayoutGraph(false);
                            if (Animation)
                            {
                                Area2.GenerateGraph(true, true);
                                Area.GenerateGraph(true, true);
                                await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                            }
                            string temp = list[i].Text;
                            list[i].Text = list[parent].Text;
                            list[parent].Text = temp;

                            string tempText = curBlock.Text;
                            curBlock.Text = parentBlock.Text;
                            parentBlock.Text = tempText;


                            if (Animation)
                            {
                                Area2.GenerateGraph(true, true);
                                Area.GenerateGraph(true, true);
                                await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                            }

                            list[i].Color = Brushes.LightGray;
                            list[parent].Color = Brushes.LightGray;
                            curBlock.Background = Brushes.White;
                            parentBlock.Background = Brushes.White;
                            if (Animation)
                            {
                                Area2.GenerateGraph(true, true);
                                Area.GenerateGraph(true, true);
                            }

                            i = parent;
                            parent = (i - 1) / 2;
                        }

                        Area2.GenerateGraph(true, true);
                        Area.GenerateGraph(true, true);
                        list1[indexInFirstHeap * 2 + 2].Color = Brushes.Red;

                    }

                    Area2.GenerateGraph(true, true);
                    Area.GenerateGraph(true, true);
                }

                var resultVertex = Area2.LogicCore.Graph.Vertices.ToList()[0];
                resultVertex.Color = Brushes.DeepPink;
                TextBlock resultBlock = (((ArrayPanel2.Children[0] as StackPanel).Children[0] as Border).Child as TextBlock);
                resultBlock.Background = Brushes.DeepPink;
                TextBlock resultBlockInFirstGraph = (((ArrayPanel.Children[Convert.ToInt32(resultVertex.Text.Split(';')[1])] as StackPanel).Children[0] as Border).Child as TextBlock);
                resultBlockInFirstGraph.Background = Brushes.DeepPink;
                Area.LogicCore.Graph.Vertices.ToList()[Convert.ToInt32(resultVertex.Text.Split(';')[1])].Color = Brushes.DeepPink; ;
            }
            else
            {
                KIndex.Background = Brushes.Red;
            }
                  

            Area2.GenerateGraph(true, true);
            Area.GenerateGraph(true, true);
            AnimationProcessEnd();
        }   

        private void Clean()
        {
            WrongKIndexBorder.BorderBrush = Brushes.White;
            WrongNumberBorder.BorderBrush = Brushes.White;
            WrongSizeBorder.BorderBrush = Brushes.White;
            DeleteButton.Background = Brushes.LightGray;

            for (int i = 0; i < ArrayPanel.Children.Count; i++) {
                (((ArrayPanel.Children[i] as StackPanel).
                    Children[0] as Border).Child as TextBlock).Background = Brushes.White;
            }
            var list = Area.LogicCore.Graph.Vertices.ToList();
            for (int i = 0; i < list.Count; i++) {
                list[i].Color = Brushes.LightGray;
            }
            
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var list = Area.LogicCore.Graph.Vertices.ToList();
            Clean();

            if (list.Count > 0)
            {
                ExtractMin(Area, ArrayPanel);
            }
            else {
                DeleteButton.Background = Brushes.Red;
            }
        }

        private async void ExtractMin(GraphAreaExample area, StackPanel arrayPanel) {

            var graph = area.LogicCore.Graph;
            var list = graph.Vertices.ToList();

            if (graph.VertexCount > 0)
            {
                AnimationProcessStart();

                TextBlock parentBlock = (((arrayPanel.Children[0] as StackPanel).Children[0] as Border).Child as TextBlock);
                TextBlock sonBlock = (((arrayPanel.Children[graph.VertexCount - 1] as StackPanel).Children[0] as Border).Child as TextBlock);
                

                DataVertex dataVertex = list[0];
                DataVertex lastVertex = list[graph.VertexCount - 1];

                dataVertex.Color = Brushes.Red;
                lastVertex.Color = Brushes.Yellow;
                parentBlock.Background = Brushes.Red;
                sonBlock.Background = Brushes.Yellow;

                if (Animation)
                {
                    area.GenerateGraph(true, true);
                    await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                }

                string temp = dataVertex.Text;
                dataVertex.Text = lastVertex.Text;
                lastVertex.Text = temp;

                dataVertex.Color = Brushes.Yellow;
                lastVertex.Color = Brushes.Red;

                string tempBlockText = parentBlock.Text;
                parentBlock.Text = sonBlock.Text;
                sonBlock.Text = tempBlockText;

                parentBlock.Background = Brushes.Yellow;
                sonBlock.Background = Brushes.Red;

                if (Animation)
                {
                    area.GenerateGraph(true, true);
                    await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                }

                arrayPanel.Children.RemoveAt(graph.VertexCount - 1);
                graph.RemoveVertex(lastVertex);

                if (Animation)
                {
                    area.GenerateGraph(true, true);
                    await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                }

                AnimationProcessEnd();

                if (area.LogicCore.Graph.VertexCount > 0)
                {
                    Down(area, arrayPanel, dataVertex, 0);
                }

                area.GenerateGraph(true, true);
            }

        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            MyStream.Save(Area.LogicCore.Graph.Vertices.
                Select(x => Convert.ToInt32(x.Text)).ToList());
        }

        private async void Open_Click(object sender, RoutedEventArgs e)
        {
            List<int> input = MyStream.Open();
            AnimationProcessStart();
            Clean();
            ArrayPanel.Children.RemoveRange(0, ArrayPanel.Children.Count);
            Area.LogicCore.Graph = new GraphExample();
            var graph = Area.LogicCore.Graph;
            //var list = graph.Vertices.ToList();
            
            for (int counter = 0; counter < input.Count; counter++)
            {
                var dataVertex1 = new DataVertex(input[counter].ToString());

                //Add vertex to data graph
                graph.AddVertex(dataVertex1);
                StackPanel stackPanel = new StackPanel();
                Border border = new Border();
                border.BorderThickness = new Thickness(1);
                border.BorderBrush = Brushes.Black;
                border.Child = new TextBlock()
                {
                    Text = input[counter].ToString(),
                    Width = 50,
                    Height = 50,
                    Padding = new Thickness(20)
                };

                stackPanel.Children.Add(border);
                stackPanel.Children.Add(new TextBlock()
                {
                    Text = (graph.VertexCount - 1).ToString(),
                    Foreground = Brushes.Blue,
                    Margin = new Thickness(25, 15, 25, 15)
                });
                ArrayPanel.Children.Add(stackPanel);
            }
            var list = Area.LogicCore.Graph.Vertices.ToList();
            
            for (int counter = 0; counter < graph.VertexCount; counter++)
            {
                int parent1 = (counter - 1) / 2;
                if (parent1 != counter) {
                    var dataEdge = new DataEdge(list[parent1], list[counter]);
                    graph.AddEdge(dataEdge);
                }
            }

            Area.GenerateGraph(true, true);

            //for (int index = (graph.VertexCount - 2) / 2 + 1; index >= 0; index--)
            //{
            //    waitHandle.Add(new ManualResetEvent(false));
            //}

            //waitHandle[(graph.VertexCount - 2) / 2 + 1].Set();
            for (int index = (graph.VertexCount - 2) / 2; index >= 0; index--)
            {
                //waitHandle[index + 1].WaitOne();
                //Down(Area, ArrayPanel, Area.LogicCore.Graph.Vertices.ToList()[index], index);
                int current = index;
                while (true)
                {
                    int minIndex = current;
                    int leftSon = current * 2 + 1;
                    int rightSon = current * 2 + 2;

                    DataVertex CurrentVertex = list[current];
                    CurrentVertex.Color = Brushes.Blue;

                    if (Animation)
                    {
                        Area.GenerateGraph(true, true);
                        await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                    }

                    if (leftSon < graph.VertexCount
                        && GetNumber(list[leftSon]) < GetNumber(list[minIndex]))
                    {
                        DataVertex prevMinVertex = list[minIndex];
                        prevMinVertex.Color = Brushes.LightGray;
                        minIndex = leftSon;
                        DataVertex nextMinVertex = list[minIndex];
                        nextMinVertex.Color = Brushes.Orange;
                        if (Animation)
                        {
                            Area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                        }
                    }

                    if (rightSon < graph.VertexCount
                        && GetNumber(list[rightSon]) < GetNumber(list[minIndex]))
                    {
                        DataVertex prevMinVertex = list[minIndex];
                        prevMinVertex.Color = Brushes.LightGray;
                        minIndex = rightSon;
                        DataVertex nextMinVertex = list[minIndex];
                        nextMinVertex.Color = Brushes.Orange;
                        if (Animation)
                        {
                            Area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                        }
                    }

                    TextBlock parentBlock = (((ArrayPanel.Children[current] as StackPanel).Children[0] as Border).Child as TextBlock);
                    TextBlock sonBlock = (((ArrayPanel.Children[minIndex] as StackPanel).Children[0] as Border).Child as TextBlock);
                    DataVertex parentVertex = list[current];
                    DataVertex sonVertex = list[minIndex];

                    if (minIndex != current)
                    {
                        parentBlock.Background = Brushes.Green;
                        sonBlock.Background = Brushes.Green;
                        parentVertex.Color = Brushes.Green;
                        sonVertex.Color = Brushes.Green;

                        if (Animation)
                        {
                            Area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                        }

                        string temp2 = parentVertex.Text;
                        parentVertex.Text = sonVertex.Text;
                        sonVertex.Text = temp2;

                        string tempText = parentBlock.Text;
                        parentBlock.Text = sonBlock.Text;
                        sonBlock.Text = tempText;

                        parentVertex.Color = Brushes.LightGray;
                        sonVertex.Color = Brushes.Orange;

                        parentBlock.Background = Brushes.White;
                        sonBlock.Background = Brushes.Orange;
                        if (Animation)
                        {
                            Area.GenerateGraph(true, true);
                            await Task.Delay(Convert.ToInt32(1000 / SpeedSlider.Value));
                        }
                    }
                    else
                    {
                        list[current].Color = Brushes.LightGray;
                        parentBlock.Background = Brushes.White;
                        break;
                    }
                    current = minIndex;
                }
            }
            //waitHandle = new List<ManualResetEvent>();
            Area.GenerateGraph(true, true);
            AnimationProcessEnd();
        }
    }
}

